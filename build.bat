set project_name=travian_analyze
set 7z="C:\Program Files\7-Zip\7z.exe"

del /S /Q "dist\%project_name%\*"

:: Build application:
pyinstaller --hidden-import queue --hidden-import travian4api -F main.py

:: Copy files to project dir:
copy /Y config.debug.cfg "dist\%project_name%\config.cfg"
copy /Y dist\main.exe "dist\%project_name%\%project_name%.exe"

:: Build .zip file:
"C:\Program Files\7-Zip\7z.exe" u -tzip -mx9 -r0 "%cd%\dist\%project_name%.zip" "%cd%\dist\%project_name%"
