# Анализатор альянса для Travian Legends #

## Цели проекта

Создание инструмента для анализа активности игроков определенного альянса.

## Зависимости:

* travian4api

## Сборка проекта

### Для сборки нужны инструменты:

* python3 (version <= 3.4.4)
* pyinstaller
* travian4api

### Windows

В папке проекта выполнить в cmd:

`build.bat`

В папке dist будет создан исполняемый файл. Также будет создана папка и .zip-архив со всеми компонентами нужными для выполнения.

Для запуска приложения нужно выполнить travian_analyze.exe

### Linux

В терминале выполнить:

`make`