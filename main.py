import sys
import os.path
import time
import configparser
import random

import travian4api

CONFIG_FILE = 'config.cfg'
DEBUG_CONFIG_FILE = 'config.debug.cfg'

ONLINE_REPR = ['В сети', "Не в сети", "Нет 3 дня", "Нет 7 дней"]


def load_config():
    config = configparser.ConfigParser()

    if os.path.exists(DEBUG_CONFIG_FILE):
        config.read(DEBUG_CONFIG_FILE)
        print('Debug mode')
    else:
        config.read(CONFIG_FILE)

    return config


def open_account(url, username, password):
    account = travian4api.Account(url, username, password)

    if account.login.login():
        print('Login success!')
    else:
        print('Login failed!')

    return account


def write_data(account, config):
    output_file = config['OUTPUT']['file_path']

    with open(output_file, 'a') as file:
        alliance = account.alliance
        loctime = time.strftime('%d-%m-%Y %H:%M:%S')
        alliance_name = alliance['name']
        data = 'time: {}, alliance: {}\n'.format(
            loctime,
            alliance_name)
        print(data, end='')
        file.write(data)
        members = alliance['members']
        for member in members:
            username = member['name']
            online_id = member['online']
            online = ONLINE_REPR[online_id - 1]
            data = 'user: {}, online: {}\n'.format(
                username,
                online)
            print(data, end='')
            file.write(data)
        file.write('\n')


def main():
    config = load_config()

    url = config['USER']['server_url']
    username = config['USER']['username']
    password = config['USER']['password']

    print('Url:', url)
    print('User:', username)

    SLEEP_MIN = config['SYSTEM'].getfloat('time_sleep')
    TIME_RANDOM_RANGE_PERCENT = config['SYSTEM'].getfloat('sleep_random_range_percent')

    account = open_account(url, username, password)

    while True:
        write_data(account, config)
        sleep_time = (1 + TIME_RANDOM_RANGE_PERCENT*(2*random.random() - 1)) * SLEEP_MIN
        print('Sleep {} min'.format(sleep_time))
        time.sleep(sleep_time * 60)  # convert min to sec


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit()
